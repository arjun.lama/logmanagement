/*
 * This file contain all the actions that are performed in wrangling  and categorizing the events for better understanding
 */
package processlogfiles;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Controller {
	private Scanner input;
	private List<PolicyEvent> policies;
	private List<InventoryEvent> inventories;
	private List<SoftwareUpdateEvent> updates;
	private List<Computer> computersWithEvents;
	
	public Controller() {
		policies=new ArrayList<PolicyEvent>();
		inventories=new ArrayList<InventoryEvent>();
		updates=new ArrayList<SoftwareUpdateEvent>();
		computersWithEvents=new ArrayList<Computer>();
	}
	
	/*check if file is existing and can be opened*/
	private boolean isExisting(String filename) {
		File file=new File(filename);
		
		if(file.exists())
			return true;
		return false;
	}
	
	//get file
	private File getFile() {
		input=new Scanner(System.in);
		//get user file
		System.out.print("Enter File Name: ");
		String filename=input.nextLine();
		
		while(!this.isExisting(filename)) {
			System.out.printf("File with the name [ %s ] does not exist !\n",filename);
			System.out.printf("\nEnter File Name: ");
			filename=input.next();
			
		}
		File file=new File(filename);
		
		return file;
	}
	
	/*read the text files containing the events*/
	private void readLog(File filename) {
		//open the file using the Scanner API
		try {
			input=new Scanner(filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
					
		//read file
		while(input.hasNext()) {
			//split output based on white spaces 
			String logs[]=input.nextLine().split(" ");
		
			String time,compName,event;
			if(logs.length>1) {
				time=logs[0];
				compName=logs[1];
				event=logs[2];
				
				//get computers with events
				computersWithEvents.add(new Computer(time,compName,event));
				
				//separate based on event
				switch(event) {
					case "POLICY":
						String policyNumber=logs[3];
						String status=logs[4];
						policies.add(new PolicyEvent(time,new Computer(time,compName,event),policyNumber,status));
						break;
					case "SOFTWAREUPDATES":
						String updateNumber=logs[3];
						String updateStatus=logs[4];
						updates.add(new SoftwareUpdateEvent(time,new Computer(time,compName,event),updateNumber,updateStatus));
						break;
					case "INVENTORY":
						String type=logs[3];
						String actionStatus=logs[4];
						inventories.add(new InventoryEvent(time,new Computer(time,compName,event),type,actionStatus));
						break;
					}
				}
			}
	}
	
	private Map<String,List<String>> getMachineEvent(String computerName){
		Map<String,List<String>>compEvents=new HashMap<>();
		List<String> events=new ArrayList<>();
		
		for(InventoryEvent event:inventories) {
			if(event.getComputer().getName().equals(computerName)) {
				String e=event.getEventTime()+" "+event.getComputer().getName()+" "+event.getComputer().getEvent()+" "+event.getType()+" "+event.getActionStatus();
				events.add(e);
			}
		}
		for(PolicyEvent event:policies) {
			if(event.getComputer().getName().equals(computerName)) {
				String e=event.getEventTime()+" "+event.getComputer().getName()+" "+event.getComputer().getEvent()+" "+event.getNumber()+" "+event.getStatus();
				events.add(e);
			}
		}
		for(SoftwareUpdateEvent event:updates) {
			if(event.getComputer().getName().equals(computerName)) {
				String e=event.getEventTime()+" "+event.getComputer().getName()+" "+event.getComputer().getEvent()+" "+event.getUpdateNumber()+" "+event.getSystemUpdateStatus();
				events.add(e);
			}
		}
		
		compEvents.put(computerName,events);
		
		return compEvents;
	}
	//get failed update events
	private List<String>getFailedUpdates(){
		List<String>failedUpdates=new ArrayList<>();
		
		for(SoftwareUpdateEvent updateEvent:updates) {
			if(updateEvent.getSystemUpdateStatus().equals("Failed")) {
				String e=updateEvent.getComputer().getName()+" : "+"update "+updateEvent.getUpdateNumber();
				failedUpdates.add(e);
			}
		}
		return failedUpdates;
	}
	//get failed inventory events
	private List<String>getFailedInventories(){
		List<String>failedInventories=new ArrayList<>();
		
		for(InventoryEvent inventory:inventories) {
			if(inventory.getActionStatus().equals("interrupted")) {
				String e=inventory.getComputer().getName()+" : "+inventory.getType()+ " "+inventory.getActionStatus();
				failedInventories.add(e);
			}
		}
		return failedInventories;
	}
	//get only names of computers with events
	private Set<String>computerNames(){
		Set<String>computers=new HashSet<>();
		for(Computer computer:this.computersWithEvents)
			computers.add(computer.getName());
		
		return computers;
	}
	//save specific computer events to file
	private void saveEventToFile(String name,Map<String,List<String>>events) {
		String filename=name+"-report.txt";
		Formatter file;
		List<String> values=null;
		try {
			file=new Formatter(filename);
			
                        for (Map.Entry<String, List<String>> entry : events.entrySet()) {
                                values= entry.getValue();
                        }
                        
			for(String event:values) {
				file.format("%s\n", event);
				file.flush();
			}
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//menu
	private void userMenu() {
            try {
                    System.out.println("Select Action\n1 - List All Computer Events\n2 - Event Report for specific computer\n3 - Show Failed Events\n0 - Exit");
                    input=new Scanner(System.in);
                    System.out.print(": ");
                    int op=input.nextInt();

                    switch(op) {
                            case 1:
                                    System.out.println("\nComputers With Events");
                                    int i=1;
                                    for(String computer:computerNames()) {
                                            System.out.printf("%3s%d : %6s\n","",i,computer);
                                            i++;
                                    }

                                    System.out.println("Computer && the Assocciated Events");
                                    for(Computer comp:computersWithEvents) {
                                            System.out.println(comp);
                                    }
                                    System.out.println();
                                    userMenu();
                                    break;
                            case 2:
                                    System.out.print("Enter Computer Name: ");
                                    String computerName=input.next();
                                    Map<String,List<String>>events=getMachineEvent(computerName);

                                    System.out.printf("\nEvents for machine '%s'",computerName);
                                    for (Map.Entry<String, List<String>> entry : events.entrySet()) {
                                        for(String v:entry.getValue()) {
                                                System.out.println(v);
                                        }
                                    }
                                    System.out.println("End of events");
                                    System.out.println("\n____________________________________");
                                    String filename=computerName+"-report.txt";
                                    System.out.println("Saving To File ["+filename+"] .*-*-*-*");
                                    this.saveEventToFile(computerName, events);
                                    System.out.println();
                                    userMenu();
                                    System.out.println();
                                    break;
                            case 3:
                                    System.out.println("Software Updates that failed");
                                    for(String failUpdate:this.getFailedUpdates())
                                            System.out.println(failUpdate);

                                    System.out.println();

                                    System.out.println("Inventory Actions that failed to complete");
                                    for(String failedInventory:this.getFailedInventories())
                                            System.out.println(failedInventory);

                                    System.out.println();
                                    userMenu();
                                    break;
                            case 0:
                                    //Exit
                                    System.out.println("Bye!\nHave a Nice Day!");
                                    break;
                            default:
                                    System.out.println("Invalid Input. Try Again!!!");
                                    break;

                    }
                } catch(InputMismatchException e ) {
                    System.out.println("Invalid Input. Try Again!!!!");
                }
	}
	
	//execute the application
	public void run() {
		System.out.println("Computer Log Files Management System\n");
		
		//get file;
		readLog(this.getFile());
		
		//Menu
		this.userMenu();
	}
}












