/**
 * This class holds the information pertaining to the information about the inventory events that occurs during an event
 */
package processlogfiles;

/**
 * @author sir
 *
 */
public class InventoryEvent extends Event {
	private String type; //software or hardware
	private String actionStatus; //completed or interrupted
	private Computer computer; //computer event occurred on
	
	/**
	 * @param time
	 * @param type
	 * @param actionStatus
	 * @param computer
	 */
	public InventoryEvent(String time, Computer computer, String type, String actionStatus) {
		super(time);
		this.type = type;
		this.actionStatus = actionStatus;
		this.computer = computer;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @return the actionStatus
	 */
	public String getActionStatus() {
		return actionStatus;
	}
	/**
	 * @return the computer
	 */
	public Computer getComputer() {
		return computer;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @param actionStatus the actionStatus to set
	 */
	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}
	/**
	 * @param computer the computer to set
	 */
	public void setComputer(Computer computer) {
		this.computer = computer;
	}
}
