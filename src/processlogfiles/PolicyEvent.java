package processlogfiles;
/**
 *  This file stores the information pertaining to the occurring policy events
 */

/**
 * @author sir
 *
 */
public class PolicyEvent extends Event {
	private String number; //number associated with the policy
	private String status; //activated or not
	private Computer computer; //computer applied to
	/**
	 * @param time
	 * @param number
	 * @param status
	 * @param computer
	 */
	public PolicyEvent(String time, Computer computer,String number, String status) {
		super(time);
		this.number = number;
		this.status = status;
		this.computer = computer;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @return the computer
	 */
	public Computer getComputer() {
		return computer;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @param computer the computer to set
	 */
	public void setComputer(Computer computer) {
		this.computer = computer;
	}

}
