package processlogfiles;
/*
 * This is a base class that contains the time which is inherited by all the occuring events
 */
public class Event {
	private String eventTime;
	
	//Initialize the event time
	public Event(String time) {
		this.eventTime=time;
	}

	/**
	 * @return the eventTime
	 */
	public String getEventTime() {
		return eventTime;
	}

	/**
	 * @param eventTime the eventTime to set
	 */
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

}
