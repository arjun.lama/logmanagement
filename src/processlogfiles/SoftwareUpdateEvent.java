/**
 * 
 */
package processlogfiles;

/**
 * @author sir
 *
 */
public class SoftwareUpdateEvent extends Event {
	private String updateNumber; //number specifying the set of updates /*For simplicity purposes, 
												/*the number is captured as a string since no multiplication is to be done on it*/
	private String systemUpdateStatus; //software update status==> non-compliant or compliant or failed
	private Computer computer; //computer associated with the update
	/**
	 * @param time
	 * @param updateNumber
	 * @param systemUpdateStatus
	 * @param computer
	 */
	public SoftwareUpdateEvent(String time, Computer computer, String updateNumber, String systemUpdateStatus) {
		super(time);
		this.updateNumber = updateNumber;
		this.systemUpdateStatus = systemUpdateStatus;
		this.computer = computer;
	}
	/**
	 * @return the updateNumber
	 */
	public String getUpdateNumber() {
		return updateNumber;
	}
	/**
	 * @return the systemUpdateStatus
	 */
	public String getSystemUpdateStatus() {
		return systemUpdateStatus;
	}
	/**
	 * @return the computer
	 */
	public Computer getComputer() {
		return computer;
	}
	/**
	 * @param updateNumber the updateNumber to set
	 */
	public void setUpdateNumber(String updateNumber) {
		this.updateNumber = updateNumber;
	}
	/**
	 * @param systemUpdateStatus the systemUpdateStatus to set
	 */
	public void setSystemUpdateStatus(String systemUpdateStatus) {
		this.systemUpdateStatus = systemUpdateStatus;
	}
	/**
	 * @param computer the computer to set
	 */
	public void setComputer(Computer computer) {
		this.computer = computer;
	}
}
