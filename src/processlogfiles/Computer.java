package processlogfiles;
/**
 * This file will contain the information about the available computers
 */

/**
 * @author sir
 *
 */
public class Computer extends Event{
	private String name;
	private String event; //event recorded by the machine
	
	
	/**
	 * @param time
	 * @param name
	 * @param event
	 */
	public Computer(String time, String name, String event) {
		super(time);
		this.name = name;
		this.event = event;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @return the event
	 */
	public String getEvent() {
		return event;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(String event) {
		this.event = event;
	}

	@Override
	public String toString() {
		return getEventTime()+" "+getName()+ " "+ getEvent();
	}
}
